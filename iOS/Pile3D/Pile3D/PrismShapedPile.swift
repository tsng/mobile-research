//
//  PrismShapedPile.swift
//  Pile3D
//
//  Created by Vinay on 7/31/20.
//  Copyright © 2020 Telesense. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

class PrismShapedPile: NSObject {
  func setupPile3D(sceneView: SCNView) -> Void {
//    let sceneFromfile = SCNScene.init(named: "AirWave.scn")
//    let tower = sceneFromfile?.rootNode.childNode(withName: "CenterTower", recursively: true)
//    let cylinder = tower?.childNode(withName: "cylinder", recursively: true)
//    let particle = cylinder?.childNode(withName: "particles", recursively: true)
//    print(particle?.pivot)
//
//
//    sceneView.scene = sceneFromfile
//    return

    let scene = SCNScene()
    scene.rootNode.addChildNode(createCameraNode())
    scene.rootNode.addChildNode(createLightNode(lightType: .ambient))
    sceneView.scene = scene

    let pileWidthFeet : CGFloat = 100.0
    let pileLengthFeet : CGFloat = 60.0
    let pileHeightFeet : CGFloat = 40.0

    let widthInMeters : CGFloat = pileWidthFeet / 3.2808
    let lengthInMeters : CGFloat = pileLengthFeet / 3.2808
    let spearHeightInMeters : CGFloat = 2.0
    let pileHeightInMeters : CGFloat = pileHeightFeet / 3.2808
    let sideWallElevation = pileHeightInMeters * 0.2

    let floor : SCNFloor = SCNFloor()
    floor.reflectivity = 0.0
    floor.width = widthInMeters * 1.2
    floor.length = lengthInMeters * 1.2
    floor.firstMaterial?.diffuse.contents = UIColor.init(red: 233/255, green: 177/255, blue: 8/255, alpha: 1.0)

    let floorNode = SCNNode.init(geometry: floor)
    floorNode.physicsBody = SCNPhysicsBody.static()


    // Create pile
    let scale : CGFloat = 0.25
    let pileNode : SCNNode = SCNNode.init()
    pileNode.position = SCNVector3(0, 0, -10.0)
    pileNode.opacity = 0.5
    pileNode.scale = SCNVector3.init(scale, scale, scale)

    pileNode.addChildNode(floorNode)

    // Place fan tubes.
    let fanNode1 = createFanTubes(position: SCNVector3.init(-widthInMeters * 0.8, 0, lengthInMeters * 0.6), length: lengthInMeters, isFrontFacing: true)
    pileNode.addChildNode(fanNode1)

    let fanNode2 = createFanTubes(position: SCNVector3.init(-widthInMeters * 0.4, 0, -lengthInMeters * 0.6), length: lengthInMeters, isFrontFacing: false)
    pileNode.addChildNode(fanNode2)

    let fanNode3 = createFanTubes(position: SCNVector3.init(widthInMeters * 0.4, 0, lengthInMeters * 0.6), length: lengthInMeters, isFrontFacing: true)
    pileNode.addChildNode(fanNode3)

    let fanNode4 = createFanTubes(position: SCNVector3.init(widthInMeters * 0.8, 0, -lengthInMeters * 0.6), length: lengthInMeters, isFrontFacing: false)
    pileNode.addChildNode(fanNode4)

    let frontAirIntakeNode = createAirIntakeNode(width: widthInMeters, length: lengthInMeters, height: pileHeightInMeters, elevation: sideWallElevation, isFront: true)
    pileNode.addChildNode(frontAirIntakeNode)

    let backAirIntakeNode = createAirIntakeNode(width: widthInMeters, length: lengthInMeters, height: pileHeightInMeters, elevation: sideWallElevation, isFront: false)
    pileNode.addChildNode(backAirIntakeNode)

    let particleDieFloor : SCNFloor = SCNFloor()
    particleDieFloor.reflectivity = 0.0
    particleDieFloor.width = widthInMeters * 1.2
    particleDieFloor.length = lengthInMeters * 1.2
    particleDieFloor.firstMaterial?.diffuse.contents = UIColor.clear

    let angle = atan((pileHeightInMeters - sideWallElevation) / lengthInMeters)
    let spreadingAngle : CGFloat = 60 // 180 - (2 * angle * 180 / CGFloat.pi)

    let topAirDeflectorNode = createAirDeflectorNode(width: widthInMeters,
                                                     length: 0,
                                                     height: pileHeightInMeters,
                                                     spreadingAngle: spreadingAngle,
                                                     endParticleNode: floorNode)
    pileNode.addChildNode(topAirDeflectorNode)

    let length1 = lengthInMeters * 0.66
    let height1 = tan(angle) * (lengthInMeters - length1) + sideWallElevation

    let topAirDeflectorNode1 = createAirDeflectorNode(width: widthInMeters,
                                                      length: -length1,
                                                      height: height1,
                                                      spreadingAngle: spreadingAngle,
                                                      endParticleNode: floorNode)
    pileNode.addChildNode(topAirDeflectorNode1)

    let topAirDeflectorNode2 = createAirDeflectorNode(width: widthInMeters,
                                                      length: length1,
                                                      height: height1,
                                                      spreadingAngle: spreadingAngle,
                                                      endParticleNode: floorNode)
    pileNode.addChildNode(topAirDeflectorNode2)

    let length2 = lengthInMeters * 0.33
    let height2 = tan(angle) * (lengthInMeters - length2) + sideWallElevation

    let topAirDeflectorNode3 = createAirDeflectorNode(width: widthInMeters,
                                                      length: -length2,
                                                      height: height2,
                                                      spreadingAngle: spreadingAngle,
                                                      endParticleNode: floorNode)
    pileNode.addChildNode(topAirDeflectorNode3)

    let topAirDeflectorNode4 = createAirDeflectorNode(width: widthInMeters,
                                                      length: length2,
                                                      height: height2,
                                                      spreadingAngle: spreadingAngle,
                                                      endParticleNode: floorNode)
    pileNode.addChildNode(topAirDeflectorNode4)

    let spacing : CGFloat = 5.0
    for wi in stride(from: -widthInMeters + spacing, to: widthInMeters - spacing, by: spacing) {
      for li in stride(from: -lengthInMeters + spacing, to: lengthInMeters - spacing, by: spacing) {
        let height = tan(angle) * (lengthInMeters - abs(li)) + sideWallElevation

        let spearValues = [91.0, 55.0, 45.5]
        let spearNode : SCNNode = createSpearNode(values: spearValues, position: SCNVector3.init(wi, height + 0.2, li), height:  spearHeightInMeters)
        pileNode.addChildNode(spearNode)
      }
    }

    let pileShape = createGrainPilePrismStyle(width: widthInMeters, length: lengthInMeters, height: pileHeightInMeters, elevation: sideWallElevation)
    pileShape.opacity = 0.5
    pileNode.addChildNode(pileShape)

    // Add hotspot.
    let hotspot = createHotspotNode(position: SCNVector3.init(12.0, 6.0, 0.0), radius: 2.0, color: UIColor.red)
    pileNode.addChildNode(hotspot)

    // Add cake
    let cakePhysicalBox : SCNBox = SCNBox.init(width: 6, height: 3, length: 5, chamferRadius: 1.0)
    let physicalBoxShae : SCNPhysicsShape = SCNPhysicsShape.init(geometry: cakePhysicalBox, options: [SCNPhysicsShape.Option.type : SCNPhysicsShape.ShapeType.boundingBox])
    let physicalBody : SCNPhysicsBody = SCNPhysicsBody.init(type: SCNPhysicsBodyType.static, shape: physicalBoxShae)

//    let cakeBox : SCNBox = SCNBox.init(width: 6, height: 3, length: 5, chamferRadius: 1.0)
//    cakeBox.firstMaterial?.diffuse.contents = UIColor.gray
//    let cakeNode : SCNNode = SCNNode.init(geometry: cakeBox)
//    cakeNode.physicsBody = physicalBody
//    cakeNode.position = SCNVector3.init(-9.0, 6.0, 0.0)
//    pileNode.addChildNode(cakeNode)

    sceneView.scene?.rootNode.addChildNode(pileNode)
  }

  func createHotspotNode(position: SCNVector3, radius: CGFloat, color: UIColor) -> SCNNode {
    let hotspotBall = SCNSphere.init(radius: radius)
    hotspotBall.firstMaterial?.diffuse.contents = color
    hotspotBall.firstMaterial?.metalness.contents = color
    hotspotBall.firstMaterial?.emission.contents = color

    let hotspotNode = SCNNode()
    hotspotNode.geometry = hotspotBall
    hotspotNode.castsShadow = false
    hotspotNode.position = position;

    let fadeInAction : SCNAction = SCNAction.fadeIn(duration: 1.0)
    let fadeOutAction : SCNAction = SCNAction.fadeOut(duration: 1.0)

    let sequenceOfActions : SCNAction = SCNAction.sequence([fadeOutAction, fadeInAction])

    let repeatingAction: SCNAction = SCNAction.repeatForever(sequenceOfActions)

//    hotspotNode.runAction(repeatingAction)

//    let filter : CIFilter = CIFilter(name: "CIGaussianBlur")!
//    filter.setValue(0.5, forKey: kCIInputRadiusKey)
//    hotspotNode.filters = [filter]


    return hotspotNode
  }

  func createAirDeflectorNode(width: CGFloat,
                              length: CGFloat,
                              height: CGFloat,
                              spreadingAngle : CGFloat,
                              endParticleNode: SCNNode?) -> SCNNode {
      let vertices: [SCNVector3] = [
          // 4 corners elevated
          SCNVector3(-width, height, length), // 0 -> Left top.
          SCNVector3(width, height,  length), // 1 -> Right top
        ]
      let geometrySource = SCNGeometrySource(vertices: vertices)

        let indices: [UInt16] = [
          0, 1
        ]
        let geometryElement = SCNGeometryElement(indices: indices, primitiveType: .line)

        let geometry = SCNGeometry.init(sources: [geometrySource], elements: [geometryElement])
        geometry.firstMaterial?.diffuse.contents = UIColor.clear

        let lineNode : SCNNode = SCNNode()
        lineNode.geometry = geometry
        lineNode.position = SCNVector3.init(0, 0, 0)

    let particleVelocity : CGFloat = 1.0
      // Assign particle systems.
      for i in stride(from: 1.0, through: 2 * width, by: 1.0) {
        let particleLifeSpan : CGFloat = 3.0 // height / particleVelocity
        let particleNode = createParticleNode(
          position: SCNVector3.init(i - width, height, length),
          emitingDirection: SCNVector3.init(0.0, -1.0, 0.0),
          particleLifeSpan: particleLifeSpan,
          spreadingAngle: spreadingAngle,
          particleVelocity: particleVelocity,
          speedFactor: 1.0,
          birthRate: 1.0,
          emissionDuration: 0.1,
          image: createImageForSize(size: CGSize.init(width: 50, height: 10), color: UIColor.white),
          endParticleNode: endParticleNode,
          hotspotNode: nil,
          cakeNode: nil)

        lineNode.addChildNode(particleNode)
      }

        return lineNode
  }

  func createAirIntakeNode(width: CGFloat, length: CGFloat, height: CGFloat, elevation: CGFloat, isFront: Bool) -> SCNNode {
    let vertices: [SCNVector3] = [
        // 4 corners elevated
        SCNVector3(-width, elevation, isFront ? length : -length), // 0 -> Left elevation.
        SCNVector3(width, elevation,  isFront ? length : -length), // 1 -> Right elevation
      ]
    let geometrySource = SCNGeometrySource(vertices: vertices)

      let indices: [UInt16] = [
        0, 1
      ]
      let geometryElement = SCNGeometryElement(indices: indices, primitiveType: .line)

      let geometry = SCNGeometry.init(sources: [geometrySource], elements: [geometryElement])
      geometry.firstMaterial?.diffuse.contents = UIColor.clear

      let lineNode : SCNNode = SCNNode()
      lineNode.geometry = geometry
      lineNode.position = SCNVector3.init(0, 0, 0)

    let angle = atan((height - elevation) / length)

    // Assign particle systems.
    for i in stride(from: 1.0, through: 2 * width, by: 1.0) {
      let particleNode = createParticleNode(position: SCNVector3.init(i - width, elevation, isFront ? length : -length),
                                            emitingDirection: SCNVector3.init(0.0, 0.0, isFront ? -1.0 : 1.0),
                                            particleLifeSpan: 5.0,
                                            spreadingAngle: 0,
                                            particleVelocity: 1.0,
                                            speedFactor: 1.0,
                                            birthRate: 1.0,
                                            emissionDuration: 0.1,
                                            image: UIImage.init(named: "Air.png")!,
                                            endParticleNode: nil,
                                            hotspotNode: nil,
                                            cakeNode: nil)
      particleNode.eulerAngles = SCNVector3.init(isFront ? angle : -angle, 0.0, 0.0)
      lineNode.addChildNode(particleNode)
    }

      return lineNode
  }

  func createParticleNode(position: SCNVector3,
                          emitingDirection: SCNVector3,
                          particleLifeSpan: CGFloat,
                          spreadingAngle : CGFloat,
                          particleVelocity : CGFloat,
                          speedFactor : CGFloat,
                          birthRate : CGFloat,
                          emissionDuration : CGFloat,
                          image: UIImage,
                          endParticleNode: SCNNode?,
                          hotspotNode: SCNNode?,
                          cakeNode: SCNNode?) -> SCNNode {
    let particleNode = SCNNode.init()

    let particleSystem : SCNParticleSystem = SCNParticleSystem.init()

    particleSystem.birthRate = birthRate
    particleSystem.birthLocation = .vertex
    particleSystem.birthDirection = .surfaceNormal

    particleSystem.emissionDuration = emissionDuration
    particleSystem.emittingDirection = emitingDirection

    particleSystem.particleVelocity = particleVelocity
    particleSystem.particleLifeSpan = particleLifeSpan
    particleSystem.speedFactor = speedFactor
    particleSystem.spreadingAngle = spreadingAngle

    particleSystem.particleImage = image
    particleSystem.loops = true

    particleSystem.blendMode = SCNParticleBlendMode.alpha
    particleSystem.particleSize = 0.01
    particleSystem.stretchFactor = 0.0

    if let node = endParticleNode {
      particleSystem.colliderNodes?.append(node)
    }
    if let node = hotspotNode {
      particleSystem.colliderNodes?.append(node)
    }
    if let node = cakeNode {
      particleSystem.colliderNodes?.append(node)
    }
    particleSystem.particleDiesOnCollision = true
    particleSystem.isAffectedByPhysicsFields = true

    particleNode.addParticleSystem(particleSystem)
    particleNode.position = position

    return particleNode
  }

  func createFanTubes(position: SCNVector3, length: CGFloat, isFrontFacing: Bool) -> SCNNode {
    let outerRadius = 0.5
    let geometry = SCNTube.init(innerRadius: CGFloat(outerRadius / 2.0), outerRadius: CGFloat(outerRadius), height: length)
    geometry.firstMaterial?.diffuse.contents = UIColor.brown

    let tubeNode = SCNNode.init(geometry: geometry)
    tubeNode.position = position
    tubeNode.position.y = Float(outerRadius)
    tubeNode.eulerAngles = SCNVector3.init(90 * Float.pi / 180, 0, 0)

    let particleNode = SCNNode.init()

    particleNode.position = SCNVector3.init(0.0, 0.0, 0.0)

    let particleSystem : SCNParticleSystem = SCNParticleSystem.init()

    particleSystem.birthRate = 1
    particleSystem.birthLocation = .vertex
    particleSystem.birthDirection = .surfaceNormal

    particleSystem.emissionDuration = 0.1
    particleSystem.emittingDirection = SCNVector3.init(0.0, isFrontFacing ? 1.0 : -1.0, 0.0)

    particleSystem.particleVelocity = 1.0
    particleSystem.particleLifeSpan = 4.0
    particleSystem.speedFactor = 1.0
    particleSystem.spreadingAngle  = 2.0
    
    particleSystem.particleImage = createImageForSize(size: CGSize.init(width: 50, height: 10), color: UIColor.init(red: 0.8, green: 0.0, blue: 0.2, alpha: 1.0))
    particleSystem.particleColor = UIColor.init(white: 1.0, alpha: 0.5)
    particleSystem.loops = true

    particleSystem.blendMode = SCNParticleBlendMode.alpha
    particleSystem.particleSize = 0.01
    particleSystem.stretchFactor = 0.0

    particleNode.addParticleSystem(particleSystem)

    tubeNode.addChildNode(particleNode)

    return tubeNode
  }

  func createGrainPilePrismStyle(width: CGFloat, length: CGFloat, height: CGFloat, elevation: CGFloat) -> SCNNode {

    let wallColor = UIColor.init(red: 1.0, green: 0.5, blue: 0.5, alpha: 1.0)

    let pileNode : SCNNode = SCNNode()
    pileNode.castsShadow = true
    pileNode.position = SCNVector3.init(0, 0, 0)
    pileNode.opacity = 1.0

      let vertices: [SCNVector3] = [
        // 4 corners
        SCNVector3(-width, 0, -length), // 0 -> TL
        SCNVector3(-width, 0, length), // 1 -> BL
        SCNVector3(width, 0, -length), // 2 -> TR
        SCNVector3(width, 0, length), // 3 -> BR

        // 4 corners elevated
        SCNVector3(-width, elevation, -length), // 4 -> TLE
        SCNVector3(-width, elevation, length), // 5 -> BLE
        SCNVector3(width, elevation, -length), // 6 -> TRE
        SCNVector3(width, elevation, length), // 7 -> BRE

        // Top of the pile.
        SCNVector3(-width, height, 0), // 8 -> left top.
        SCNVector3(width, height, 0), // 9 -> right top.
      ]
    let geometrySource = SCNGeometrySource(vertices: vertices)

      let leftFrontWallIndices: [UInt16] = [
        // left front wall.
        1, 4, 0,
        1, 5, 4,
      ]
      let leftFrontWallGeometryElement = SCNGeometryElement(indices: leftFrontWallIndices, primitiveType: .triangles)

      let leftFrontWallGeometry = SCNGeometry.init(sources: [geometrySource], elements: [leftFrontWallGeometryElement])
      leftFrontWallGeometry.firstMaterial?.diffuse.contents = wallColor

    let leftFrontWallNode = SCNNode.init(geometry: leftFrontWallGeometry)
    pileNode.addChildNode(leftFrontWallNode)


    let rightFrontWallIndices: [UInt16] = [
      // right front wall.
      2, 6, 3,
      6, 7, 3,
    ]
    let rightFrontWallGeometryElement = SCNGeometryElement(indices: rightFrontWallIndices, primitiveType: .triangles)
    let rightFrontWallGeometry = SCNGeometry.init(sources: [geometrySource], elements: [rightFrontWallGeometryElement])
    rightFrontWallGeometry.firstMaterial?.diffuse.contents = wallColor

    let rightFrontWallNode = SCNNode.init(geometry: rightFrontWallGeometry)
    pileNode.addChildNode(rightFrontWallNode)

    let backWallIndices: [UInt16] = [
      // back wall.
      6, 2, 0,
      0, 4, 6,
    ]
    let  backWallGeometryElement = SCNGeometryElement(indices: backWallIndices, primitiveType: .triangles)

    let  backWallGeometry = SCNGeometry.init(sources: [geometrySource], elements: [backWallGeometryElement])
    backWallGeometry.firstMaterial?.diffuse.contents = wallColor

    let backWallNode = SCNNode.init(geometry: backWallGeometry)
    pileNode.addChildNode(backWallNode)

    let frontWallIndices: [UInt16] = [
      // Front wall.
      1, 3, 7,
      7, 5, 1,
    ]
    let frontWallGeometryElement = SCNGeometryElement(indices: frontWallIndices, primitiveType: .triangles)

    let frontWallGeometry = SCNGeometry.init(sources: [geometrySource], elements: [frontWallGeometryElement])
    frontWallGeometry.firstMaterial?.diffuse.contents = wallColor

    let frontWallNode = SCNNode.init(geometry: frontWallGeometry)
    pileNode.addChildNode(frontWallNode)

    let leftFrontFaceIndices: [UInt16] = [
      // left front face.
      4, 5, 8,
    ]
    let leftFrontFaceGeometryElement = SCNGeometryElement(indices: leftFrontFaceIndices, primitiveType: .triangles)

    let leftFrontFaceGeometry = SCNGeometry.init(sources: [geometrySource], elements: [leftFrontFaceGeometryElement])
    leftFrontFaceGeometry.firstMaterial?.diffuse.contents = wallColor

    let leftFrontFaceNode = SCNNode.init(geometry: leftFrontFaceGeometry)
    pileNode.addChildNode(leftFrontFaceNode)

    let rightFrontFaceIndices: [UInt16] = [
      // Right front face.
      7, 6, 9,
    ]
    let rightFrontFaceGeometryElement = SCNGeometryElement(indices: rightFrontFaceIndices, primitiveType: .triangles)

    let rightFrontFaceGeometry = SCNGeometry.init(sources: [geometrySource], elements: [rightFrontFaceGeometryElement])
    rightFrontFaceGeometry.firstMaterial?.diffuse.contents = wallColor

    let rightFrontFaceNode = SCNNode.init(geometry: rightFrontFaceGeometry)
    pileNode.addChildNode(rightFrontFaceNode)

    let backFaceIndices: [Int32] = [
      // Back face.
      4, 4, 8, 9, 6,
    ]
    let backFaceIndexData = Data(bytes: backFaceIndices, count: backFaceIndices.count * MemoryLayout<Int32>.size)
    let backFaceGeometryElement = SCNGeometryElement(data: backFaceIndexData, primitiveType: .polygon, primitiveCount: 1, bytesPerIndex: MemoryLayout<Int32>.size)

    let backFaceGeometry = SCNGeometry.init(sources: [geometrySource], elements: [backFaceGeometryElement])
    backFaceGeometry.firstMaterial?.diffuse.contents = wallColor

    let backFaceNode = SCNNode.init(geometry: backFaceGeometry)
    pileNode.addChildNode(backFaceNode)

    let frontFaceIndices: [Int32] = [
      // Front face,
      4, 5, 7, 9, 8,
    ]
    let frontFaceIndexData = Data(bytes: frontFaceIndices, count: frontFaceIndices.count * MemoryLayout<Int32>.size)
    let frontFaceGeometryElement = SCNGeometryElement(data: frontFaceIndexData, primitiveType: .polygon, primitiveCount: 1, bytesPerIndex: MemoryLayout<Int32>.size)

    let frontFaceGeometry = SCNGeometry.init(sources: [geometrySource], elements: [frontFaceGeometryElement])
    frontFaceGeometry.firstMaterial?.diffuse.contents = wallColor

    let frontFaceNode = SCNNode.init(geometry: frontFaceGeometry)
    pileNode.addChildNode(frontFaceNode)

    pileNode.name = "prism"
      return pileNode
    }

  func createSpearNode(values: Array<Double>, position: SCNVector3, height: CGFloat) -> SCNNode {
    let shaftValues: Array<Double> = Array(values[0...1])
    let image = createImageForValues(values: shaftValues, width: 0, height: 0)

    let shaft = SCNCylinder(radius: 0.1, height: height)

    let topMaterial = SCNMaterial();
    topMaterial.diffuse.contents = UIColor.gray

    let sidesMaterial = SCNMaterial();
    sidesMaterial.diffuse.contents = image

    let bottomMaterial = SCNMaterial();
    bottomMaterial.diffuse.contents = UIColor.gray

    shaft.materials = [sidesMaterial, topMaterial, bottomMaterial]

    // Create spear
    let shaftNode : SCNNode = SCNNode()
    shaftNode.geometry = shaft
    shaftNode.castsShadow = false
    shaftNode.position = SCNVector3.init(0.0, -1.2, 0.0)

    // Create bulb.
    let bulbColor = getColorForTemp(temp: values[2])
    let bulbSphere = SCNSphere.init(radius: 0.2)
    bulbSphere.firstMaterial?.diffuse.contents = bulbColor

    let bulbNode = SCNNode()
    bulbNode.geometry = bulbSphere
    bulbNode.castsShadow = false
    bulbNode.position = position;

    bulbNode.addChildNode(shaftNode)

    return bulbNode
  }

  func createCameraNode() -> SCNNode {
    let cameraNode = SCNNode()
    cameraNode.camera = SCNCamera()
    cameraNode.position = SCNVector3(x: 0, y: 0, z: 10)

    return cameraNode
  }
  func createLightNode(lightType: SCNLight.LightType) -> SCNNode {
    let light = SCNLight()
    light.color = UIColor.white
    light.type = lightType
    light.intensity = 1500 // Default SCNLight intensity is 1000

    let lightNode = SCNNode()
    lightNode.light = light

    return lightNode
  }
}
