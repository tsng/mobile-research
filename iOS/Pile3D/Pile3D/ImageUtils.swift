//
//  ImageUtils.swift
//  Pile3D
//
//  Created by Vinay on 7/31/20.
//  Copyright © 2020 Telesense. All rights reserved.
//

import Foundation
import UIKit

func createImageForValues(values : Array<Double>, width: CGFloat, height: CGFloat) -> UIImage {
  // Pixel to meter formula:
  // 1 m = 3779.5275590551

  // Pixel to inch formula
  // 1 in = 96 pixel (X)

  let radius : CGFloat = width > 0 ? width : 2 * 96 * 0.01
  let spearHeight : CGFloat = height > 0 ? height : 2 * 3779.5275590551 * 0.01

  var colors : Array = Array<CGColor>.init()
  for j in 0..<values.count {
    let color = getColorForTemp(temp: values[j])
    colors.append(color.cgColor)
  }
  let renderer = UIGraphicsImageRenderer.init(size: CGSize.init(width: radius * 2, height: spearHeight))
  let image = renderer.image { (context: UIGraphicsImageRendererContext) in
    let flip : CGAffineTransform = CGAffineTransform.init(scaleX: 1.0, y: -1.0)
    let flipThenShift : CGAffineTransform = flip.translatedBy(x: 0, y: -spearHeight)
    context.cgContext.concatenate(flipThenShift)

    let colorSpace = CGColorSpaceCreateDeviceRGB()
    let colorLocations: [CGFloat] = getColorDistribution(count: values.count)
    let gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: colorLocations)

    let startPoint = CGPoint.zero
    let endPoint = CGPoint(x: 0, y: spearHeight)
    context.cgContext.drawLinearGradient(gradient!, start: startPoint, end: endPoint,options: [])
  }
  return image
}

func getColorForTemp(temp: Double) -> UIColor {
  let val = 1023 * temp / 86;
  if val < 256 {
    return UIColor.init(red: 0, green: CGFloat(val / 255.00), blue: 1.0, alpha: 1.0)
  } else if val < 512 {
    let newVal = val - 256
    return UIColor.init(red: 0, green: 1.0, blue: CGFloat((255.0 - newVal) / 255.00), alpha: 1.0)
  } else if val < 768 {
    let newVal = val - 512
    return UIColor.init(red: CGFloat(newVal / 255.0), green: 1.0, blue: 0, alpha: 1.0)
  } else {
    let newVal = val - 768
    return UIColor.init(red: 1.0, green: CGFloat((255.0 - newVal) / 255.00), blue: 0, alpha: 1.0)
  }
}

func getColorDistribution(count: Int) -> Array<CGFloat> {
  if count <= 2 {
    return [0.0, 1.0]
  }
  let increments = 1.0 / CGFloat(count - 1);
  var arr: [CGFloat] = Array.init()
  arr.append(0.0)
  for i in 1..<count - 1 {
    arr.append(CGFloat(i) * increments)
  }
  arr.append(CGFloat(1.0))
  return arr
}

func createImageForSize(size : CGSize, color: UIColor) -> UIImage {
  let renderer = UIGraphicsImageRenderer.init(size: size)
  let image = renderer.image { (context: UIGraphicsImageRendererContext) in
    let flip : CGAffineTransform = CGAffineTransform.init(scaleX: 1.0, y: -1.0)
    let flipThenShift : CGAffineTransform = flip.translatedBy(x: 0, y: -size.height)
    context.cgContext.concatenate(flipThenShift)

    color.setFill()
    context.fill(CGRect.init(origin: CGPoint.zero, size: size))
  }
  return image
}
