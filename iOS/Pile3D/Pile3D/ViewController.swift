//
//  ViewController.swift
//  Pile3D
//
//  Created by Vinay on 7/24/20.
//  Copyright © 2020 Telesense. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {

  var pileNode : SCNNode?
  var screenCenter: CGPoint?

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.

    let sceneView = self.view as! SCNView

    //    addSilo()
    let pileBuilder = PrismShapedPile()
    pileBuilder.setupPile3D(sceneView: sceneView)

    sceneView.showsStatistics = true
    sceneView.antialiasingMode = .multisampling4X
    sceneView.autoenablesDefaultLighting = true
    sceneView.preferredFramesPerSecond = 60
    sceneView.allowsCameraControl = true

    DispatchQueue.main.async {
      self.screenCenter = self.view.bounds.mid
    }

    if let camera = sceneView.pointOfView?.camera {
        camera.wantsHDR = true
        camera.wantsExposureAdaptation = true
        camera.exposureOffset = -1
        camera.minimumExposure = -1
    }
  }

  func addSilo() {
    addSiloFromScene2()
  }

  func addSiloFromScene2() -> Void {
    let sceneView = self.view as! SCNView

    let scene = SCNScene.init(named: "Prism.scn")
    sceneView.scene = scene
  }

  func addSiloProgrammatically()-> Void {
    let sceneView = self.view as! SCNView
    sceneView.scene = SCNScene()

  }

  func addSiloFromScene1() -> Void {
    let sceneView = self.view as! SCNView

    let scene = SCNScene.init(named: "GrainPile.scn")
    sceneView.scene = scene
  }

}

extension CGRect {

  var mid: CGPoint {
    return CGPoint(x: midX, y: midY)
  }
}
